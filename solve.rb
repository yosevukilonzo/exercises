def checkered_board(dimension)
  black = "\u25A1"
  white = "\u25A0"
  even = (((black + " " + white + " ") * (dimension * 0.5)).rstrip + "\n")
  even_2 = (((white + " " + black + " ") * (dimension * 0.5)).rstrip + "\n")
  odd = white + " " + even
  odd_2 = black + " " + even_2

  if !dimension.is_a?(Integer) || dimension < 2
    return false
  elsif (dimension % 2 == 0)
    board = (even + even_2) * (dimension * 0.5)
    return board.rstrip
  elsif dimension % 2 == 1
    board = (odd + odd_2) * (dimension * 0.5) + odd
    return board.rstrip
  end

end

p checkered_board(4)
# puts checkered_board(5)
# "\u25A1 \u25A0\n\u25A0 \u25A1"
# "\u25A1 \u25A0 \n\u25A0 \u25A1"
