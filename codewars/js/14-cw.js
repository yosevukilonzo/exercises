//  TODO


//  Split the Bill
var people = {A: 20.63, B: 15.15, C: 10.20};

function splitTheBill(persons) {
  var total = 0;
  var count = 0;
  var average = 0;
  // var currentPerson;

  for (person in persons) {
    total += persons[person];
    console.log(total);
    count +=1;
  }
  average = total / count;
  console.log(average);

  for (person in persons) {
    persons[person] -= average;
    // Why couldn't I use Math.round?
    // Why does + change the string to a number?
    persons[person] = +persons[person].toFixed(2);
    console.log(persons[person]);
  }
  return persons
}
console.log(splitTheBill(people));

/*  Is Undefined?
- Remove undefinded values from array
  - modify a callback, used on each array element
- Finish isUndefined, which returns true if value is Undefined
- check if value is undefined,
  - yes: remove
  - no: increment to next element
*/

/*
var arr = [1, ,];
isUndefined()
*/
// isUndefined v1 - Why doesn't this work?
var arr = [1, ,];
function isUndefined(value) {
  if (value == undefined) {
    return true;
  }
}

console.log(isUndefined(arr[1]));

// isUndefined v2 - Why does this work?
// redo with typeof
function isUndefined(value) {
  if (value != undefined) {
    return false;
  }
  else {
    return true;
  }
}



//  Lazily executing a function, closure?
//  http://www.codewars.com/kata/lazily-executing-a-function/train/javascript
function add(a, b) {
  return a + b;
}

var make_lazy = function(passedFunction, par1, par2) {
  return passedFunction(par1, par2);
};

console.log(make_lazy(add, 1, 2));


//  Lazily executing a function, closure?
//  http://www.codewars.com/kata/lazily-executing-a-function/train/javascript
//  http://www.w3schools.com/js/js_function_invocation.asp
//  TODO Learn more about args object and apply()
function add(num1, num2) {
  return num1 + num2;
}

var make_lazy = function() {
  var func = arguments[0];
  var args = [];

  for (var i = 1; i < arguments.length; i++) {
      args.push(arguments[i]);
  }
  // What am I returning?
  return func.apply(this, args);
};
// var lazy_value = make_lazy(add, 1, 2);
// console.log(make_lazy(add, 1, 2));
var lazy_sum = make_lazy(add, 1, 2);
console.log(lazy_sum);

// TODO: Make lazy_value a function
// The value of this, when used in a function, is the object that "owns" the function.
// v3
/*
I didn't completely understand this one until I reviewed the arguments object
and apply(). I then worked everything out except the closure with the apply()
I didn't realize that I had to return the applied function within an anonymous function!
*/
// http://stackoverflow.com/questions/28124828/pass-a-closure-with-function-apply
function add(a, b) {
  return a + b;
}

var make_lazy = function() {
  var func = arguments[0];
  var args = [];

  for (var i = 1; i < arguments.length; i++) {
    args.push(arguments[i]);
  }
  return function() { // This is the part that stumped me!
    return func.apply(this, args);
  }
};

var lazy_value = make_lazy(add, 1, 2);
console.log(lazy_value());

// v2
// var add = function(num1, num2) {
//   return num1 + num2;
// }
//
// var make_lazy = function() {
//   var func = arguments[0];
//   console.log(func);
//   var args = [];
//
//   for (var i = 1; i < arguments.length; i++) {
//       args.push(arguments[i]);
//   }
//   console.log(args);
//   // Save to lazy_value need a closure!
//   return func.apply(this, args); // this.apply(this, args)
// };
// // Make lazy_value a function!
// var lazy_value = make_lazy(add, 2, 3);
// console.log(lazy_value);

// v1 mess
// function add(num1, num2) {
//   return num1 + num2;
// }
//
// var make_lazy = function() {
//   var func = arguments[0];
//   var args = [];
//
//   for (var i = 1; i < arguments.length; i++) {
//       args.push(arguments[i]);
//   }
//   // What am I returning?
//   return func.apply();
// };
// // var lazy_value = make_lazy(add, 1, 2);
// // console.log(make_lazy(add, 1, 2));
// var lazy_sum = make_lazy(add, 1, 2);
// console.log(lazy_sum());


// call
// var obj = {num:2};

// var addToThis = function(a){
  // return this.num + a;
// }

// addToThis.call(obj, 3); //  functionName.call(obj, functionArs)

// apply array of args instead of args themselves


// Build a deck:
var buildDeck = function(){
  // Is there a quick way to get a range of numbers e.g. 2-10?
  var cardValues = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"];
  var suits = ["d", "h", "c", "s"];
  var builtDeck = [];

  for (var i = 0; i < cardValues.length; i++) {
    for (var j = 0; j < suits.length; j++) {
      builtDeck.push(cardValues[i] + suits[j]);
    }
  }
  return builtDeck;
};
var newDeck = buildDeck();
// Stack a deck:
var StackedDeck = function(cheatCode) {
  // conditionals for cheatCode
  // Does this refer to newDeck when it is assigned?
    // this.shuffleDeck() = [];
};

// Add a shuffle function to a StackedDeck object prototype:
StackedDeck.prototype.shuffleDeck = function() {
  var shuffledCards = [];
  var randomCard;

  while(this.length != 0) {
    randomCard = Math.floor(Math.random() * this.length);
    shuffledCards.push(this.splice(randomCard, 1));
  }
  return shuffledCards;
}


newDeck = new StackedDeck();
console.log(newDeck.shuffleDeck());
// shuffleDeck(["a", "b", "c", "d"]);
// Confusion: passing functions and setting functions in prototype to be called by object
// console.log(newDeck);
