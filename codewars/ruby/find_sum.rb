=begin
- subarray index i = 0
- diagonal index d = 0
- for each i
  - add d to sum
  - increment d

-
=end
# Block: how to do it in three lines


# def diagonalSum(matrix)
#   sum, d = 0, 0
#   matrix.each do |i|
#     sum += d
#     d++
#   end
#   p sum
# end

def diagonalSum(matrix)
  sum = 0
  matrix.each_with_index.map { |arr, i| sum += arr[i] }
end

# matrix = [[1, 0, 0, 0],
#           [0, 1, 0, 0],
#           [0, 0, 1, 0],
#           [0, 0, 0, 1]]
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

diagonalSum(matrix)

# Version 3
def diagonalSum(matrix)
  matrix.map.with_index { |a, i| a[i] }.reduce(:+)
end

# Version 4
require 'matrix'
def diagonalSum(matrix)
  Matrix[*matrix].trace
end
