# Return the length of the longest palindrome in `s`
# a palindrome is the same word when reversed

# define longest as the first char with lengh = 1
# check for longer palindromes by incrementing length of the substring to checking
  # stop checking when longest.length is equal to the length of the remaining characters to check because then there cannot be a longer palindrome.
# check for a longer palindrome based on the second char
  # repeat same procedure as with first char
# stop checking when the length of the remainding substring from the current char index is equal to the longest palindrome

# def longest_palindrome s
#   longest = ''
#   # s.each_char do |char|
#     char_index = 0
#     substring = ''
#     # puts longest.length
#     puts substring
#     puts substring.reverse
#     while longest.length <  s.length
#       puts 'New loop'
#       substring << s[char_index]
#       puts substring
#       puts char_index
#       puts "longest: #{longest}"
#       if substring == substring.reverse && substring.length > longest.length
#         puts 'New longest'
#         longest = substring
#       end
#       char_index += 1
#     end
#   # end
# end

# Find the length of the longest palindrome substring

# loop: set start of substring to first char, second char etc.
  # loop condition
  # if substring is a palindrome
    # set substring to longest palindrome
  # add next char to substring

# def longest_palindrome s
#   # start = 0
#   substring = ''
#   longest = ''
#
#   s.each_char do |c|
#     substring << c
#     puts
#     puts "substring: #{substring}"
#     if substring == substring.reverse
#       longest = substring
#       puts "reversed: #{substring.reverse}"
#       puts "palindrome: #{longest}"
#     else
#       puts "don't assign to longest"
#     end
#   end
#   puts longest
#
#
#   # start += 1
# end
# should return 7

def longest_palindrome s
  #find longest palindrome in substring starting at index i
  i = 0
  j = i + 1
  longest = ''

  while i <= s.length
    puts
    puts "outer"
    sub = s[i]

    while j <= s.length - 1
      puts
      puts "inner #{s[j]}"
      sub << s[j]
      puts sub == sub.reverse
      puts sub.length > longest.length
      if sub == sub.reverse && sub.length > longest.length
        puts sub
        longest = sub
        puts longest
      end
      j += 1
    end
    i += 1
  end
end

# longest_palindrome('abba')
longest_palindrome('aabba')
# longest_palindrome('I like racecars that go fast')



def longest_palindrome s
  return 0 if s.length < 1
  longest = s[0]
  # loop
  s_arr = s.split('')
  s_arr.each_index do |i|
    # set current string
    current_str = ''
    sub_arr = s_arr.slice(i, s_arr.length)
    # loop
    sub_arr.each do |char|
      # current_str << char
      current_str += char
      if current_str == current_str.reverse
        if current_str.length > longest.length
          longest = current_str
        end
      end
    end
  end
  puts longest
  puts longest.length
end

# longest_palindrome('abba')
longest_palindrome('aabba')
longest_palindrome('baablkj12345432133d')
