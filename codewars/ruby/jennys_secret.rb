def greet(name)
  name == "Johnny" ? "Hello, my love!" : "Hello, #{name}!"
end

# Version 2
# def greet(name)
#   "Hello, #{name == 'Johnny' ? 'my love' : name}!"
# end
