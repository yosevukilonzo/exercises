def count_by(multiple, number)
  multiples = []
  start = 0
  number.times do
    start += multiple
    multiples << start
  end
  puts multiples
end

count_by(2, 5)

# Version 1

def count_by(x, n)
  (1..n).map{|i| i*x}
end
