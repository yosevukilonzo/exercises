class Complement
  @DNA_nucleotides = ['G', 'C', 'T', 'A']
  @RNA_nucleotides = ['C', 'G', 'A', 'U']
  
  def self.of_dna(dna)
    if dna.match(/[^GCTA]/)
      ''
    else
      dna.chars.map { |char| char = @RNA_nucleotides[@DNA_nucleotides.index(char)] }.join
    end
  end
end

Complement.of_dna('GX')
module BookKeeping
  VERSION = 4
end
