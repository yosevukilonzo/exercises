class Complement
  @DNA_nucleotides = ['G', 'C', 'T', 'A']
  @RNA_nucleotides = ['C', 'G', 'A', 'U']
  # @complements = { G: 'C', C: 'G', T: 'A', A: 'U' }
  def self.of_dna(dna)
    # If DNA includes partially invalid input
    if dna.match(/[^GCTA]/)
      ''
    else
    # p (@DNA_nucleotides - dna.chars)
    # p @complements.keys - dna.chars
      dna.chars.map { |char| char = @RNA_nucleotides[@DNA_nucleotides.index(char)] }.join
    end
  end
end

Complement.of_dna('GX')
module BookKeeping
  VERSION = 4
end
