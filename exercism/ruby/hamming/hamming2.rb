class Hamming
  def self.compute(a, b)
    if a.length != b.length
      fail ArgumentError, "Sequences must be of equal length"
    else
      # 0.upto(a.length)
      (0..a.length).count do |i|
        a[i] != b[i]
      end
    end
  end
end

module BookKeeping
  VERSION = 3
end
