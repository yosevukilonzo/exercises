/*
* Problem
    1. Convert date to Roman numeral
      * Date is day of month as integer
      * Roman numeral is 1 <= date <= 31
    2. Reverse order of digits from conversion
      * if result is a valid Roman numeral
        * Convert back to Arabic numeral
      * else return -1
    * Example
      6: // 4
      7: // -1 

* Pseudocode
    1. Convert date to Roman numeral v1
      * if date / 30 != 0
        * XXX+I
     * else if date / 20 != 0
        * XX+
        * call converter again
     * else if date / 10 != 0
        * X+
        * call converter again
     * else 
        * if date / 5 != 0
            if date < 9
              V+I++
            else
              return IX
        * else
          * if date < 4
              I++
            else
              return IV
    1. Convert date to Roman numeral v2
      * base = date / 10
      * rmdr = date % 10
      * if base == 3
          XXX + convert rmdr
      * if base == 2
          XX + convert rmdr
      * if base == 1
          X + convert rmdr
      * else
        * if rmdr > 5
            rmdr == 9 ? IX:V+I++
        * else
            rmdr == 4 ? IV:I++ 
    2. Reverse numeral
    3. Check if valid
			* Valid if in in this order: X V I except for  IV and IX
				['I', 'V', 'X] valid orders: 000, 01, 02
      * if valid, return Arabic numeral
					Convert reversed back to Arabic numeral
      * else return -1
			
		* Difficult part: clarifying what to pass into contertToRomanNumeral
*/

var dateEncryption = function(date) {
  var rmdr = date % 10; // 1
  var base = (date / 10 * 10) - rmdr;
  var numerals = '';
	var reversed = '';
  var convertToRoman = function(number) {
    console.log("converting " + date + " to Roman numeral");
		// number is 30 or greater
    if (number >= 30) {
      console.log(">= 30");
      numerals += 'XXX';
      convertToRoman(rmdr);
    }
		// number is 20 or greater
    else if (number >= 20) {
      numerals += 'XX';
      convertToRoman(rmdr);
    }
		// number is 10 or greater
    else if (number >= 10) {
      numerals += 'X';
      convertToRoman(rmdr);
    } 
		// number is less than 10
		else {
			// number is 5 or greater
			if (rmdr / 5 >= 1) {
				// number is 9
				if (rmdr == 9) {
					numerals += 'IX';
					return numerals;
				} 
				// number is 5-8
				else {
					numerals += 'V';
					rmdr = rmdr - 5;
					convertToRoman(rmdr);
				}
			} 	
			// number is less than 5
			else {
				// number is 4
				if (rmdr == 4) {
					numerals += 'IV';
					return numerals;
				} 
				// number is 1-3
				else {
					for (var i = 0; i < rmdr; i++) {
						numerals += 'I';
					}
				}
			}  
    }
    return numerals;
  }; // Converts number
  var converted = convertToRoman(base);
	var reverseRoman = function(numeral) {
		for (var i = numeral.length - 1; i > -1; i--) {
			reversed += numeral[i];
		}
		return reversed;
	}; // Reverses numeral
	var reversed = reverseRoman(converted);
//	var checkValid = function(numeral) {
//		var numArr = ['I', 'V', 'X'];
//		for (var i = 0; i < numeral.length; i++) {
//			if (numeral[i])
//		} 
//	}; // Checks if numeral is valid
	console.log(reversed);
	return reversed;
};

dateEncryption(7);
//var secretDate = dateEncryption(3);
//var secretDate = dateEncryption(4);
//var secretDate = dateEncryption(6);
//var secretDate = dateEncryption(9);
//var secretDate = dateEncryption(14);
//var secretDate = dateEncryption(16);
//var secretDate = dateEncryption(24);
//var secretDate = dateEncryption(26);
//var secretDate = dateEncryption(29);
//var secretDate = dateEncryption(31);
//console.log(secretDate);