/* 25 + 25 min+
10. Write a JavaScript function which returns the n rows by n columns identity matrix.
- 3 x 3
1 0 0
0 1 0
0 0 1
- if [0][0], [1][1], [2][2]
- for column
*/
// Snapshot 1
// function identidyMatrix(num1, num2) {
//   for (var i = 0; i < num1; i++) {
//     for (var j = 0; j < num2; j++) {
//       if (i == j) {
//         console.log(1);
//       } else {
//         console.log(0);
//       }
//     }
//   }
// }
//
// console.log(identidyMatrix(3, 3));

// Snapshot 2
// Step 1
// function idMatrix(num1, num2) {
//   for (var i = 0; i < num1; i++) {
//     var row = "";
//     for (var j = 0; j < num2; j++) {
//       row += "x";
//     }
//     console.log(row);
//   }
// }
//
// idMatrix(3, 3);

function idMatrix(num1, num2) {
  var onePlace = 0;
  var one = "1";
  var zero = "0";

  for (var i = 0; i < num1; i++) {
    var row = "";
    for (var j = 0; j < num2; j++) {
      if (j == onePlace) {
        row += one;
      }
      else {
        row += zero;
      }
    }
    onePlace++;
    console.log(row);
  }
}

idMatrix(10, 20);
