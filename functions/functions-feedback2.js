// 7
function countVowels(string) {
  var letters = string.split('');
  var vowels = "aeiou";
  var vowelIndex;
  var vowel;
  var vowelCount = 0;
  var i = 0;

  while (i < vowels.length) {
    if (letters.includes(vowels[i]) === true) {
      // Is this:
      vowelIndex = letters.indexOf(vowels[i]);
      vowel = letters.splice(vowelIndex, 1);
      // Better than this:
      // vowel = letters.splice(letters.indexOf(vowels[i]), 1);
      vowelCount++;
    }
    else {
      i++;
    }
  }
  console.log(vowelCount);
}
countVowels("The quick brown fox");



// 8
function checkPrime(num) {
  var i = 3;

  if (num != 2 && num % 2 == 0 || num < 2) {
    return "not prime";
  }
  else {
    while (i < num) {
      if (num % i == 0) {
        return "not prime";
      }
      else {
        i += 2
      }
    }
  }
  return "prime";
}
console.log(checkPrime(1));
console.log(checkPrime(97));
console.log(checkPrime(49));
console.log(checkPrime(2));


// 12
// Sum of proper positive divisors
function checkPerfectNumber(num) {
  var sum = 0;
  for (var i = 1; i < num; i++) {
    if (num % i == 0) {
      sum += i;
    }
  }
  if (sum == num && num != 0) {
    return "perfect";
  }
  else {
    return "not perfect";
  }
}

// Half sum of all positive divisors
function checkPerfectNumber(num) {
  var i = 1;
  var check = 0;

  while (i <= num) {
    if (num % i == 0) {
      check += i;
    }
    i++;
  }
  if (check / 2 == num && num != 0) {
    return "perfect";
  }
  else {
    return "not perfect";
  }
}

console.log(checkPerfectNumber(0));
console.log(checkPerfectNumber(1));
console.log(checkPerfectNumber(6));
console.log(checkPerfectNumber(28));
console.log(checkPerfectNumber(496));
console.log(checkPerfectNumber(2389));
console.log(checkPerfectNumber(8128));

// 14
function amountToCoins(num) {
  var coins = {quarters: 0, dimes: 0, nickels: 0, pennies: 0};

  if (num > 1) {
    coins.quarters = (Math.floor(num) * 4);
    num = Math.floor(num % Math.floor(num) * 100);
  }
  coins.quarters += Math.floor((num / 100 * 4));
  num = num % 25;
  coins.dimes = Math.floor(num / 10);
  num = num % 10;
  coins.nickels = Math.floor(num / 5);
  num = num % 5;
  coins.pennies = num;
  return coins;
}
console.log(amountToCoins(223.84));



// 17
function countLetters(string) {
  var letters = string.split('');
  var i;
  var currentLetter;
  var letterCounts = {};

  while (letters.length > 0) {
    i = 0;
    currentLetter = letters[i];
    while (letters.includes(currentLetter)) {
      if (letterCounts[currentLetter] == undefined) {
        letterCounts[currentLetter] = 1;
      }
      else {
        letterCounts[currentLetter] += 1;
      }
      // letters.splice(i, 1); // Mistake
      letters.splice(letters.indexOf(currentLetter), 1); // fixed
    }
  }
  console.log(letterCounts);
}
countLetters("dcbcdbcdacdd");



// 18
function binarySearch(array, letter) {
  var i;
  var guess;

  while (array.length > 1) {
    i = Math.floor(array.length / 2);
    guess = array[i];

    if (guess > letter) {
      array = array.splice(0, i)
    }
    else if (guess < letter) {
      array = array.splice(i + 1, array.length - 1);
    }
    else {
      return "==:" + guess;
    }
  }
  return array[0];
}

var letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"];
console.log(binarySearch(letters, "b"));

// 23

function firstUniqueChar(string) {
var chars = string.split('');
var unique;
var i = 0;

  while(i < string.length) {
    if (chars.includes(chars[i]) == true) {
      unique = chars.splice(i, 1);
      if (chars.includes(unique[0]) == true) {
        chars = string.split('');
        console.log(unique + ": not unique");
      }
      else {
        return unique + ": unique";
      }
    }
    i++;
  }
}
console.log(firstUniqueChar("abacddbec"));



// 23 refactored

function firstUniqueChar(string) {
  var i = 0;
  while (i < string.length) {
    if (string.indexOf(string[i]) == string.lastIndexOf(string[i])) {
      return string[i];
    }
    else {
      i++;
    }
  }
}
firstUniqueChar("abacddbec");


// 24
function bubbleSort(numbers) {
  var i = 0;
  var j;
  var holdValue;

  while (i < numbers.length) {
    j = 0;
    while (j < numbers.length) {
      if (numbers[j] > numbers[j + 1]) {
        holdValue = numbers[j];
        numbers[j] = numbers[j + 1];
        numbers[j+1] = holdValue
      }
      j++;
    }
    i++;
  }
  console.log(numbers);
}
bubbleSort([27, 29, 31, 36, 2014, 2012, 11, 17, -0.1, 4, 2016, 1.99, 40, 120.96]);


// 26 Stuck on this one
function longestNoRepeatCharString(string) {
  var chars = string.split('');
  var i = 0;
  var current = chars[i];
  var longest = '';

  while (i < chars.length) {
    if (current.includes(chars[i+1]) == false && chars[i+1] != undefined) {
      current += chars[i+1];
      if (current.length > longest.length) {
        longest = current;
      }
    }
    else {
      current = chars[i]
    }
      i++;
  }
  console.log(longest);
}
// currently returning: 'nthe ou'
longestNoRepeatCharString("Food and beer in the outdoor swimming pool"); // 'or swim'
//repeating means anywhere in the substring instead of two in a row
//solution should be 'or swim'


// 27



// 27 longest palindrome
/*
- check whether the current substring is a palindrome starting at current index
	- check substring each time a letter is added until the end of the string
	- if the current substring is palindrome and is the longest assign it to longest
	- if the longest substring is longer than the remaining string
  	continue to check current substring
- increment the starting point index and repeat
	as long as the longest substring is less than the remaining string
*/

function longestPalindrome(string) {
  string = string.replace(/ /g, '');		// I learned a little about RegExp
  var letters = string.replace(' ', '').split('');
  var current = [];
  var checkCurrent;
  var longest;
  var i = 0;
  var j;

  while (i < letters.length) {
    current.push(letters[i]);
    j = i + 1;
    while (j < letters.length) {
      current.push(letters[j]);
      checkCurrent = string.slice(i, j+1).split('').reverse().join('');
      if (current.join('') == checkCurrent && checkCurrent.length > longest.length){
        // Mistake: assigning longest to checkCurrent instead of current
        longest = checkCurrent;
      }
      j++;
    }
    current = [];
    i++;
  }
  console.log("LONGEST: " + longest);
}

longestPalindrome("bananas racecar nurses run")
// nurses run



// 28
// Still not sure here. I will do some reading about the concepts,
// but maybe we can also discuss them during out next meeting.

// function passFunction(passedFunction) {
//   return passedFunction;
// }
//
// var greet = function() {
// 	console.log("hi");
// }
//
// passFunction(greet());

// I was also trying this, but I think I'm confusing myself:
// passFunction(function(){console.log("hi");});
// What about passing functions with params?
function hi(name) {
  console.log("Hi " + name);
}
function passFunction(passedFunction, param) {
  passedFunction(param);
}
passFunction(hi, "Yosevu");
