/* 26. ~ hard 25 + 25 + 25
Write a JavaScript function to find longest substring in a given a string without repeating characters
- Was confused with scope in this exercise
*/

// Need nested loops?
// function getLongestSubstring(string) {
//   var longestSubstring = "";
//
//   for (var i = 0; i < string.length; i++) {
//     console.log("LOOP: " + i);
//     var substring = string[i];
//
//     if (string[i] != string[i + 1]) {
//       substring += string[i + 1];
//       console.log(substring);
//       if (substring.length > longestSubstring.length) {
//         longestSubstring = substring;
//         console.log(longestSubstring); //
//       }
//     }
//     else {
//       substring = "";
//     }
//   }
//
// }
// // swimming pool
// getLongestSubstring("indoor"); // Should return "or swim "

/* 26.
Write a JavaScript function to find longest substring in a given a string without repeating characters
- Was confused with scope in this exercise
*/
//
// function longestSubstring(string) {
//   var substring = "";
//   var longest = "";
//   for (var i = 0; i < string.length; i++) {
//     // console.log("LOOP:" + i);
//     if (string[i] != string[i + 1]) { // i:n
//       if (i == 0) {
//         substring += string[i];
//       }
//       else if (i == string.length - 1) { // -1?
//         break; // better way to take care of i + 1 undefined?
//       }
//       substring += string[i + 1];
//       if (substring.length > longest.length) {
//         longest = substring;
//       }
//
//     } else {
//         substring = string[i + 1];
//     }
//   }
//   console.log("longest:: " + longest);
// }
//
// longestSubstring("Food and beer in the outdoor swimming pool"); // "er in the out do"

// function findLongestSubstring(string) {
//   var place = 0;
//   var currentSubstring = "";  // F
//   var longestSubstring = "";
//
//   while (place < string.length) {
//     // console.log("LOOP: " + place + ": " + string[place]);
//     if (string[place] != string[place - 1]) { // changed from string[place + 1]
//       currentSubstring += string[place];
//       if (currentSubstring.length > longestSubstring.length) {
//         longestSubstring = currentSubstring;
//       }
//     } else {
//       currentSubstring = string[place];
//     }
//   place++;
//   }
//   console.log(longestSubstring);
// }
// // almost: returning "er in the outd" instead of "er in the outdo"
// findLongestSubstring("Food and beer in the outdoor swimming pool"); // "er in the outdo"


function longestNoRepeatCharString(string) {
  var chars = string.split('');
  var i = 0;
  var current = chars[i];
  var longest = '';

  while (i < chars.length) {
    console.log("LOOP: " + i + ": " + chars[i]);
    if (current.includes(chars[i+1]) == false && chars[i+1] != undefined) {
      current += chars[i+1];
      console.log("current: " + current);
      if (current.length > longest.length) {
        longest = current;
        // console.log(longest);
        // console.log("remainding: " + chars.length - i);
        // console.log("longest: " + longest.length);
        // if (longest.length > chars.length - i) {
        //   console.log(longest);
        //   return longest;
        // }
      }
    }
    else {
      current = chars[i]
    }
      i++;
  }
  console.log(longest);
}
// currently returning: 'nthe ou'
longestNoRepeatCharString("Food and beer in the outdoor swimming pool"); // 'or swim'
//repeating means anywhere in the substring instead of two in a row
//solution should be 'or swim'
