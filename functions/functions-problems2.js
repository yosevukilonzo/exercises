// 6
function findLongestWord(string) {
  var words = string.split(" ");
  var longestWord = words[0];

    for (var i = 1; i < words.length; i++) {
      if (words[i].length > longestWord.length) {
        longestWord = words[i];
      }
    }
  return longestWord;
}

console.log(findLongestWord("An elephant never forgets"));



// 15
function exponentiate(b, n) {
  console.log(Math.pow(b, n));
}

exponentiate(9, 3);



// 24
function bubbleSort(numbers) {
  var currentIndex;
  for (var i = 0; i < numbers.length; i++) {
    // console.log("OUTER LOOP: " + i + ": " + numbers[i]);
    // Mistake: j = 1 instead of j = i + 1
    for (var j = i + 1; j < numbers.length; j++) {
      // console.log(" INNER LOOP: " + j + ": " + numbers[j]);
      if (numbers[i] > numbers[j]) {              // 4:5, 4:3
        currentIndex = i;
        // console.log(numbers);
        // Mistake: splicing numbers[j] instead of j
        currentIndex = numbers.splice(j, 1, numbers[i]);
        numbers.splice(i, 1, currentIndex[0]);
      }
    }
  }
  console.log(numbers);
}

bubbleSort([27, 29, 31, 36, 2014, 2012, 11, 17, -0.1, 4, 2016, 1.99, 40, 120.96]);



// 25
function longestCountryName(array) {
  var longestName = array[0];
  for (var i = 0; i < array.length; i++) {
    if (array[i].length > longestName.length){
      longestName = array[i];
    }
  }
  return (longestName);
}
console.log(longestCountryName(["Australia", "Germany", "United States of America"]));


// 26
function findLongestSubstring(string) {
  var place = 0;
  var currentSubstring = "";  // F
  var longestSubstring = "";

  while (place < string.length) {
    // console.log("LOOP: " + place + ": " + string[place]);
    if (string[place] != string[place - 1]) { // changed from string[place + 1]
      currentSubstring += string[place];
      if (currentSubstring.length > longestSubstring.length) {
        longestSubstring = currentSubstring;
      }
    } else {
      currentSubstring = string[place];
    }
  place++;
  }
  console.log(longestSubstring);
}
// Was returning "er in the outd" instead of "er in the outdo"
findLongestSubstring("Food and beer in the outdoor swimming pool"); // "er in the outdo"


// 29 v2
function doSomething() {
  console.log( arguments.callee.name);
}
doSomething();

// 29 v3
function doSomething() {
}
console.log(doSomething.name);
