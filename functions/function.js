/*

7. Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string.
Note : As the letter 'y' can be regarded as both a vowel and a consonant, we do not count 'y' as vowel here.
Example string : 'The quick brown fox'
Expected Output : 5

- set up function
- string to array
- loop: for char in string
- if char == vowel
- count++
*/

// First attempt

// function countVowels(string) {
//   var vowelCount = 0;
//   var vowels = ["a", "e", "i", "o", "u"];
//   letters = string.toLowerCase().split("");
//   // console.log(letters);
//   for (var i = 0; i < letters.length; i++) {
//     for (var j = 0; j < vowels.length; i++) {
//       if (letters[i] == vowels[j]) {
//         vowelCount += 1;
//         console.log(vowelCount);
//       }
//     }
//   }
//   return vowelCount;
//
// }

// Second attempt
// Switch or nested loop as alternative?

function countVowels(string) {
  var vowelCount = 0;
  var letters = string.toLowerCase().split("");
  console.log(letters);
  for (var i = 0; i < letters.length; i++) {
    if (letters[i] == "a" || letters[i] == "e" || letters[i] == "i" || letters[i] == "o" || letters[i] == "u") {
      vowelCount += 1;
      console.log(vowelCount);
    }
  }
  // return vowelCount;

console.log(countVowels("An elephant never forgets"));
