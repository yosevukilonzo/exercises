/* 17. 15 min
Get number of letter occurances in a string
- Make more efficient so that it doesn't count the same letter more than once
*/

function countLetters(string) {
  var letters = string.split('');
  var i;
  var currentLetter;
  var letterCounts = {};

  while (letters.length > 0) {
    i = 0;
    currentLetter = letters[i];
    while (letters.includes(currentLetter)) {
      if (letterCounts[currentLetter] == undefined) {
        letterCounts[currentLetter] = 1;
      }
      else {
        letterCounts[currentLetter] += 1;
      }
      // letters.splice(i, 1); // Mistake
      letters.splice(letters.indexOf(currentLetter), 1); // fixed
    }
  }
  console.log(letterCounts);
}

countLetters("dcbcdbcdacdd");
