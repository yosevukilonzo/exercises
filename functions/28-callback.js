// 28
// Write a JavaScript program to pass a 'JavaScript function' as parameter.

function passFunction(passedFunction) {
  return passedFunction;
}

function squareNumber(num) {
  console.log(num * num);
}

passFunction(squareNumber(8));
