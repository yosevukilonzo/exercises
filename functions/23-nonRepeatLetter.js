
/* 23. ~ 25 + 25 + 25 + 25 + 25
Write a JavaScript function to find the first not repeated character.
Sample arguments : 'abacddbec'
Expected output : 'e'


*/

// Snapshot 1
// function findNonRepeatedLetter(string) {
//   var unique;
//   for (var i = 0; i < string.length; i++) {
//     for (var j = i + 1; j < string.length; j++) {
//       if (string[i] == string[j]) {
//         unique = false;
//         break;
//       }
//     }
//     if (unique != false) {
//       return string[i];
//     }
//   }
// }
//
// console.log(findNonRepeatedLetter("abacddbec"));

// Snapshot 2
/*

*/
// Problem with wihle loop:
// function nonRepeatedChar(string) {
//   var start = 0;
//   var end = string.length - 1; // 5
//   var char = "";
//
//   while (start < end) {
//     console.log("outer loop: " + start);
//     while (end > start) {
//       console.log(start);
//       console.log(end);
//
//       console.log(" inner loop: " + end);
//       console.log("start: " + string[start]);
//       console.log("end: " + string[end]);
//
//       if (string[start] == string[end]) {
//         char = "not unique";
//         console.log(char);
//       } else {
//         char = "unique";
//         console.log(char);
//       }
//       end--;
//     }
//
//     start++;
//   }
//
// }

// for loop version
function nonRepeatedChar(string) {

  for (var i = 0; i < string.length; i++) {
    for (var j = 0; j < string.length; j++) {
      console.log("outer: " + i);
      console.log("inner: " + j + "\n");
      if (string[i] == string[j]) {
        break;
      }
      else if (j == i) {
        console.log(string[j]);
      }
      else {
        continue;
      }
    }
  }
}
nonRepeatedChar("aabcb");

// Trying to splice out and splice in an index

function nonRepeatedChar(string) {
var currentChar;
var stringArray = string.split('');
var currentStringArray = stringArray;

  for (var i = 0; i < string.length; i++) {

    currentStringArray.splice(stringArray[i], 1);
    console.log(currentStringArray);

    currentStringArray = string.split('');
    console.log(currentStringArray + "\n");
  }

}
nonRepeatedChar("aabcb");
//  [ "a", "a", "b", "c", "b" ]
