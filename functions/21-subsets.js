/* 21. ~ 25 + 25 + 25 + 25 min

Recursive solution / index-based solution

Don't include time to understand math
Write a JavaScript function to get all possible subset with a fixed length (for example 2) combinations in an array.
Sample array : [1, 2, 3] and subset length is 2
Expected output : [[2, 1], [3, 1], [3, 2], [3, 2, 1]]
- What is a power set/subset?
{1,2,3}
{}, {1}, {2}, {3}, {1,2}, {1, 3}, {2, 3}, {1, 2, 3}
*/
// Snapshot 1
// function getSubsets(array, setLength) {
//   var subsets = [];
//   for (var i = 0; i < array.length; i++) {
//     console.log("outer loop: " + i);
//     var subset = [];
//     subset.push(array[i]);
//     console.log(subset);
//     // console.log(subset.length);
//     // if (subset.length == setLength) {
//     //   subsets.push(subset);
//     //   console.log(subsets);
//     // }
//
//     for (var j = 0; j < array.length; j++) {
//       console.log("   inner loop: " + j);
//       subset.push(array[j]);
//       console.log("   " + subset);
//       // console.log(subset.length);
//       // if (subset.length == setLength) {
//       //   subsets.push(subset);
//       //   console.log(subsets);
//       // }
//
//     }
//   }
//   return subsets;
// }


function getSubsets(array, setLength) {
  var subset;
  var subsets = [];
  if (setLength == array.length) {
    subsets.push(array);
  }

  for (var i = 0; i < array.length; i++) {
    console.log("outer loop: " + i);
    subset = [];
    subset.push(array[i]);
    if (subset.length == setLength) {
      subsets.push(subset);
    }

    for (var j = i + 1; j < array.length; j++) {
      console.log("   inner loop: " + j);
      console.log(subset);
      subset.push(array[j]);
      if (subset.length == setLength) {
        // console.log(subset);
        subsets.push(subset);
      }
      // console.log(subsets);
    }

  }
  return subsets;
}
getSubsets([1, 2, 3, 4], 3);
// {1,2,3}, {1,2,4} {1,3,4}, and {2,3,4}
// console.log(getSubsets([1, 2, 3, 4, 5], 4));
