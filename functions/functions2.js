/* ~ 25 min break come back 15 min
11. Write a JavaScript function which will take an array of numbers stored and find the second lowest and second greatest numbers, respectively.
Sample array : [1,2,3,4,5]
Expected Output : 2,4

- find lowest
- find highest

*/
// Snapshot 1
// function findLowest(array) {
//   var lowest;
//   var number;
//
//   for (var i = 0; i < array.length; i++) {
//     number = array[i];
//     if (i == 0) {
//       lowest = number;
//     }
//     else if (number < lowest) {
//       lowest = number;
//     }
//   }
//   array.splice(lowest, 1); // do without splicing
// }
//
// console.log(findLowest([4, 6, 2, 8]));

// Snapshot 2
// minor sticking point: http://www.w3schools.com/jsref/jsref_sort.asp
function secLowSecHigh(array) {
  array.sort(function(a, b) {return a-b}); // Don't undertand this part
  console.log(array);
  var newArray = [];
  newArray.push(array[1]);
  newArray.push(array[array.length - 2]);
  return newArray;
}

console.log(secLowSecHigh([5, 6, 7, 8, 9, 1, 2, 3, 4, 10, 0]));


/*
12. ~ 10 min
Write a JavaScript function which says whether a number is perfect.
According to Wikipedia : In number theory, a perfect number is a positive
integer that is equal to the sum of its proper positive divisors, that is, the
sum of its positive divisors excluding the number itself (also known as its
aliquot sum). Equivalently, a perfect number is a number that is half the sum
of all of its positive divisors (including itself).

Example : The first perfect number is 6, because 1, 2, and 3 are its proper
positive divisors, and 1 + 2 + 3 = 6. Equivalently, the number 6 is equal to
half the sum of all its positive divisors: ( 1 + 2 + 3 + 6 ) / 2 = 6. The next
perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect
numbers 496 and 8128.

- find divisiors
- check if sum of divisors = number
- check if number = half sum of divisors

*/

function checkPerfectNumber(num) {
  var sum = 0;
  for (var i = 1; i < num; i++) {
    if (num % i == 0) {
      sum += i;
    }
  }
  if (sum == num) {
    console.log(num + " is a perfect number.");
  } else {
    console.log(num + " is not a perfect number.");
  }
}

checkPerfectNumber(496);


// 13. 5 min
// Write a JavaScript function to compute the factors of a positive integer.


function findPositiveFactors(num) {
  var factors = "";
  for (var i = 1; i < num + 1; i++) {
    if (num % i == 0) {
      factors += i + ", ";
    }
  }
  console.log(factors);
}

findPositiveFactors(49);


/* 14. ~ 25 + 10
Write a JavaScript function to convert an amount to coins.
Sample function : amountTocoins(46, [25, 10, 5, 2, 1])
Here 46 is the amount. and 25, 10, 5, 2, 1 are coins.
Output : 25, 10, 10, 1

- 1 5 10 25
- large to small: if amount / denom
- subtract

*/

function amountToCoins(num) {
  var quarters = 0;
  var dimes = 0;
  var nickels = 0;
  var pennies = 0;

  while (num > 0) {
    if (num >= 25) {
      quarters += 1;
      num -= 25;
    }
    else if (num >= 10) {
      dimes += 1;
      num -= 10;
    }
    else if (num >= 5) {
      nickels += 1;
      num -= 5;
    } else {
      pennies += 1;
      num -= 1;
    }
  }
  console.log("quarters: " + quarters + ", dimes: " + dimes + ", nickels: " + nickels + ", pennies: " + pennies);
}

amountToCoins(39);


/* 15. ~ 10 min
Write a JavaScript function to compute the value of bn where n is the
exponent and b is the bases. Accept b and n from the user and display the result.

-
*/

function exponentiate(b, n) {
  var product = 1;
  for (var i = 0; i < n; i++) {
    product *= b;
  }
  console.log(product);
}

exponentiate(9, 3);


/* 16. ~ 25 + 25 + 25
Write a JavaScript function to extract unique characters from a string.
Example string : "thequickbrownfoxjumpsoverthelazydog"
Expected Output : "thequickbrownfxjmpsvlazydg"

- split string
- compare index 0 to index 1
  - if equal delete index 1
  - if not increment to next index
- if no indices are equal to current index, increment index
First try was with splice
*/

// Snapshot 1
// function extractUniqueChars(string) {
//   var letters = string.split("");
//   var uniqueChars = [];
//   console.log(letters);
//
//   for (var i = 0; i < letters.length; i++) {
//     for (var j = i + 1; j < letters.length; j++) {
//       if (letters[i] != letters[j]) {
//         uniqueChars.push(letters[i]);
//         console.log(uniqueChars);
//       }
//       else if (letters[i] == letters[j]) {
//         letters.splice(letters[j], 1);
//
//       }
//     }
//   }
// }

function extractUniqueChars(string) {
  var letters = string.split("");

  for (var i = 0; i < letters.length; i++) {
    // console.log("outer loop " + i);
    for (var j = i + 1; j < letters.length; j++) {
      // console.log(" inner loop " + j);
      if (letters[i] == letters[j]) {
        // mistake using letters[j] instead of j
        letters.splice(j, 1);
      }
    }
  }
  console.log(letters.join(""));
}

extractUniqueChars("thequickbrownfoxjumpsoverthelazydog"); // thes n


/* 17. ~ 15 min
Write a JavaScript function to  get the number of occurrences of each letter in specified string.
- if index 0 == index 1, count++
- if index 0 == index 2, count ++
- ...
*/
// Snapshot 1
// Make more efficient so that it doesn't count the same letter more than once
function counterLetters(string) {
  for (var i = 0; i < string.length; i++) {
    // console.log("outer loop " + i);
    var count = 0;
    for (var j = 0; j < string.length; j++) {
      // console.log(" inner loop " + j);
      if (string[i] == string[j]) {
        count++;
      }
    }
    console.log(string[i].toUpperCase() + "s: " + count);
  }
}

counterLetters("cbcbcac");


// 19 ~ 5?

function getLargerElements(array, number) {
  largerNumbers = [];
  for (var i = 0; i < array.length; i++) {
    if (array[i] > number) {
      largerNumbers.push(array[i]);
    }
  }
  console.log(largerNumbers);
}
getLargerElements([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 8);


/*
20. ~ 25min
Write a JavaScript function that generates a string id (specified length) - what does this mean? - of random characters. Go to the editor
Sample character list : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

Generate a random string of characters of specified length:
- get a random number in the range of the char list's length
- concat the random number index to random string variable
- repeat specified times

*/
charList = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

function getRandomString(string, length) {
  randString = '';
  for (var i = 0; i < length; i++) {
    var randNum = Math.floor(Math.random() * string.length);
    randString += string[randNum];
  }
  console.log(randString);
}

getRandomString(charList, 10);
