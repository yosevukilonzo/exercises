/* 24. ~ hard 25 25min
Write a JavaScript function to apply Bubble Sort algorithm.
Note : According to wikipedia "Bubble sort, sometimes referred to as sinking sort,
is a simple sorting algorithm that works by repeatedly stepping through the list to be sorted,
comparing each pair of adjacent items and swapping them if they are in the wrong order".
Sample array : [12, 345, 4, 546, 122, 84, 98, 64, 9, 1, 3223, 455, 23, 234, 213]
Expected output : [3223, 546, 455, 345, 234, 213, 122, 98, 84, 64, 23, 12, 9, 4, 1]

*/

// function bubbleSort(numbers) {
//   for (var i = 0; i < numbers.length; i++) {
//     console.log("OUTER LOOP:" + i);
//
//     for (var j = i + 1; j < numbers.length; j++) {
//       console.log(" inner loop:" + j);
//       // 1 5 4 3 2
//       if (numbers[i] > numbers[j]) {  // i:1:5 j:2:4
//         console.log(numbers[i]);
//         place = numbers.splice(j, 1, numbers[i]);    // 5,4,3,2,1
//         console.log(place);
//         numbers.splice(i, 1, place[i]);
//         console.log(numbers);
//
//       } else {
//           continue;
//       }
//     }
//   }
// }
//
// bubbleSort([4, 5, 3, 2, 1]);

// function bubbleSort(numbers) {
//   var currentIndex;
//   for (var i = 0; i < numbers.length; i++) {
//     // console.log("OUTER LOOP: " + i + ": " + numbers[i]);
//     // Mistake: j = 1 instead of j = i + 1
//     for (var j = i + 1; j < numbers.length; j++) {
//       // console.log(" INNER LOOP: " + j + ": " + numbers[j]);
//       if (numbers[i] > numbers[j]) {              // 4:5, 4:3
//         currentIndex = i;
//         // console.log(numbers);
//         // Mistake: splicing numbers[j] instead of j
//         currentIndex = numbers.splice(j, 1, numbers[i]);
//         numbers.splice(i, 1, currentIndex[0]);
//       }
//     }
//   }
//   console.log(numbers);
// }

// impropper bubbleSort +25
// function bubbleSort(numbers) {
//   var holdValue;
//   // var j;
//
//   for (var i = 0; i < numbers.length; i++) {
//     console.log("OUTER LOOP: " + i);
//     for (var j = i+1; j < numbers.length; j++) {
//       console.log(" INNER LOOP: " + j);
//       if (numbers[i] > numbers[j]) {
//         console.log(numbers);
//         holdValue = numbers[i];
//         numbers[i] = numbers[j];
//         numbers[j] = holdValue;
//       }
//     }
//   }
// }
// bubbleSort([5, 4, 3, 2, 1])
// bubbleSort([27, 29, 31, 36, 2014, 2012, 11, 17, -0.1, 4, 2016, 1.99, 40, 120.96]);

// v3 proper bubble sort +25 +25 with mentor
function bubbleSort(numbers) {
  var i = 0;
  // var j = 0; // Mistake: j isn't reassigned to 0 within the inner while loop
  var j;
  var holdValue;

  while (i < numbers.length) {
    // console.log("OUTER LOOP: " + i);
    j = 0; // fixed
    while (numbers[j] > numbers[j + 1]) {
      // console.log(" INNER LOOP: " + j);

      // console.log(numbers[j]);
      // console.log(numbers[j+1]);
      // Incorrect
      // holdValue = numbers[j];      // 5
      // numbers[j + 1] = numbers[j]; // Mistake: why doesn't this assignment work?
      // numbers[j] = holdValue;

      // Correct
      holdValue = numbers[j];       // 5
      numbers[j] = numbers[j + 1];  // 4
      numbers[j+1] = holdValue      // 5
      // console.log(numbers[j]);
      // console.log(numbers[j+1]);
      // console.log(numbers);

      j++;
    }
    i++;
  }
  console.log(numbers);
}
bubbleSort([5, 4, 3, 2, 1]);
// bubbleSort([27, 29, 31, 36, 2014, 2012, 11, 17, -0.1, 4, 2016, 1.99, 40, 120.96]);


// v4 proper bubbleSort loop condition fix + 25
function bubbleSort(numbers) {
  var i = 0;
  var j;
  var holdValue;

  while (i < numbers.length) {
    j = 0;
    while (j < numbers.length) {
      if (numbers[j] > numbers[j + 1]) {
        holdValue = numbers[j];
        numbers[j] = numbers[j + 1];
        numbers[j+1] = holdValue
      }
      j++;
    }
    i++;
  }
  console.log(numbers);
}
bubbleSort([27, 29, 31, 36, 2014, 2012, 11, 17, -0.1, 4, 2016, 1.99, 40, 120.96]);
